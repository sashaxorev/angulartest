(function (app) {
    'use strict';

    angular
        .module('app')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope', 'TasksService'];

    function HomeCtrl($scope, TasksService) {
        var self = this;

        this.openViewTaskDialog = openViewTaskDialog;
        this.getFormattedDate = getFormattedDate;
        this.addTask = addTask;
        this.showTask = showTask;
        this.selectDate = selectDate;
        this.remove = remove;
        this.editTask = editTask;

        init();

        return this;

        function init(){
            loadTodayTasks();
            self.newDate = new Date();
            $scope.$on("apply", onModalApply);

            function onModalApply (ev, obj) {
                if(obj && obj.id == 'taskEdit') {
                    if($scope.currentTask) {
                        remove($scope.currentTask);
                        $scope.currentTask = null;
                    }

                    TasksService.addTask(
                        $scope.editedTask.title,
                        $scope.editedTask.description,
                        $scope.editedTask.date
                    );

                    updateTasks();
                }

                if(obj && obj.id == 'dateSelect') {
                    loadTasks (self.newDate);
                }
            }
        }

        function selectDate () {
            $scope.isDateSelectOpen = true;
        }

        function addTask(){
            showEditTaskDialog('Add task');
        }

        function editTask(task) {
            showEditTaskDialog('Edit task', task);
        }

        function remove(task){
            for(var i=0; i < $scope.tasks.length; i++) {
                if ($scope.tasks[i] == task) {
                    $scope.tasks.splice(i, 1);
                    break;
                }
            }
            TasksService.updateTasksForDate($scope.date, $scope.tasks);
        }

        function showTask (task) {
            $scope.editedTask = {
                title: task.title,
                description: task.description
            };

            openViewTaskDialog();
        }

        function showEditTaskDialog(title, task) {
            $scope.currentTask = task;
            $scope.taskEditTitle = title;
            $scope.editedTask = {
                title: task ? task.title : '',
                description: task ? task.description : '',
                date: $scope.date
            };
            $scope.isEditTaskOpen = true;
        }

        function openViewTaskDialog(){
            $scope.isViewTaskOpen = true;
        }

        function getFormattedDate () {
            return $scope.date ? TasksService.formatDate($scope.date) : "";
        }

        function loadTasks (date) {
            $scope.date = date;
            updateTasks();
        }

        function updateTasks () {
            $scope.tasks = TasksService.getTasksForDate($scope.date);
        }

        function loadTodayTasks () {
            loadTasks(new Date());
        }
    }

}());
