(function (app) {
    'use strict';

    angular
        .module('app')
        .factory('TasksService', TasksService);

    TasksService.$inject = ['$window'];

    function TasksService($window) {
        var service = {
            getTasksForDate: getTasksForDate,
            addTask: addTask,
            updateTasksForDate: updateTasksForDate,
            formatDate: formatDate
        };

        return service;

        function getTasksForDate(date){
            var key = getKeyForDate(date);
            var tasks = getAllNotes()[key] || [];
            return tasks;
        }

        function addTask(title, description, date) {
            var notes = getAllNotes();
            var key = getKeyForDate(date);
            var obj = {title: title, description: description};

            if(!notes[key]) {
                notes[key] = [];
            }

            notes[key].push(obj);
            saveAll(notes);
        }

        function updateTasksForDate (date, tasks) {
            var notes = getAllNotes();
            var key = getKeyForDate(date);

            notes[key] = tasks;
            saveAll(notes);
        }

        function getAllNotes () {
            var notes = $window.localStorage.getItem('tasks') || {};
            return angular.fromJson(notes);
        }

        function saveAll (tasks) {
            $window.localStorage.setItem('tasks', angular.toJson(tasks));
        }

        function getKeyForDate (date) {
            return formatDate (date);
        }

        function formatDate(date) {
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();

            month = padNumber(month);
            day = padNumber(day);

            return day + "." + month + "." + year;
        }

        function padNumber (num) {
            return num < 10 ? "0" + num : num;
        }
    }

}());