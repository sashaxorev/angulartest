(function (app) {
    'use strict';

    angular
        .module('app')
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider){
        $routeProvider
            .when('/home',{
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'ctrl'
            })
            .when('/about',{
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'ctrl'
            })
            .otherwise('/home');
    }

}());
